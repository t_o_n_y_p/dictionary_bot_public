sample.tg\_bot package
======================

Submodules
----------

sample.tg\_bot.conversation\_mode module
----------------------------------------

.. automodule:: sample.tg_bot.conversation_mode
   :members:
   :undoc-members:
   :show-inheritance:

sample.tg\_bot.core module
--------------------------

.. automodule:: sample.tg_bot.core
   :members:
   :undoc-members:
   :show-inheritance:

sample.tg\_bot.updater module
-----------------------------

.. automodule:: sample.tg_bot.updater
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: sample.tg_bot
   :members:
   :undoc-members:
   :show-inheritance:
