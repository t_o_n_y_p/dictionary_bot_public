"""
Entry point of the dictionary telegram bot application.
"""
import logging
from logging import Logger

from telegram.ext import Updater

from updater import DictionaryBotUpdater

_logger: Logger = logging.getLogger(__name__)


def main():
    """
    Launches the application.
    """
    updater: Updater = DictionaryBotUpdater()
    updater.start_polling()
    _logger.info('Telegram bot application started')
    updater.idle()


if __name__ == '__main__':
    main()
