import configparser
import gettext
import logging
from logging import Logger

import pkg_resources

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
_logger: Logger = logging.getLogger(__name__)
_logger.info('Logging settings applied')

_config = configparser.ConfigParser()
_config.read(pkg_resources.resource_filename(__name__, 'config.ini'))
_logger.info('Internal config loaded')
DB_FILE_NAME: str = _config['DEFAULT']['DatabaseFileName']

i18n = gettext.translation('messages', localedir=pkg_resources.resource_filename(__name__, 'locales'), languages=['ru'])
_logger.info('I18N settings applied')
_ = i18n.gettext
