"""
The module preforms initial operations with the database: connection, scheme init and update.
"""
import logging
from logging import Logger

import pkg_resources
from sqlalchemy import Column, String, ForeignKey, create_engine, Integer, Boolean
from sqlalchemy.engine import Engine
from sqlalchemy.orm import declarative_base, relationship, Session

from sample import DB_FILE_NAME

_logger: Logger = logging.getLogger(__name__)

Base = declarative_base()
_logger.info('Base class for table definitions created')


class WordMeaning(Base):
    """
    Stores connections between word records and meaning records.
    """
    __tablename__ = 'word_meanings'
    word_id = Column(ForeignKey('words.id'), primary_key=True)
    """Word record ID (see below)."""
    meaning_id = Column(ForeignKey('meanings.id'), primary_key=True)
    """Meaning record ID (see below)."""
    proposed_by = Column(String(32), nullable=False)
    """Telegram username of whoever proposed this word-meaning pair."""
    approved = Column(Boolean, default=False, nullable=False)
    """True if this word-meaning pair has been confirmed by the admins."""

    word = relationship('Word')
    """Related word record."""
    meaning = relationship('Meaning')
    """Related meaning record."""

    def __repr__(self):
        return f'WordMeaning({self.word_id=}, {self.meaning_id=}, {self.proposed_by=}, {self.approved=}, ' \
            + f'{self.word=}, {self.meaning=})'


class Word(Base):
    """
    Stores records of all known words.
    """
    __tablename__ = 'words'
    id = Column(Integer, primary_key=True)
    value = Column(String(32), nullable=False, unique=True, index=True)
    """Russian word, no more than 32 letters long."""

    def __repr__(self):
        return f'Word({self.id=}, {self.value=})'


class Meaning(Base):
    """
    Stores records of all known word meanings.
    """
    __tablename__ = 'meanings'
    id = Column(Integer, primary_key=True)
    value = Column(String(256), nullable=False, unique=True, index=True)
    """Meaning of a russian word, no more than 256 letters long."""

    def __repr__(self):
        return f'Meaning({self.id=}, {self.value=})'


_engine: Engine = create_engine(f'sqlite:///{pkg_resources.resource_filename(__name__, DB_FILE_NAME)}')
_logger.info('Connected to the database successfully')
Base.metadata.create_all(_engine)
_logger.info('Database scheme is up to date')


def get_session():
    return Session(_engine)
