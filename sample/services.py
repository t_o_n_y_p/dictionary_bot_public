"""
The module contains complex transaction-based methods.
"""
import logging
from logging import Logger

from sqlalchemy.exc import IntegrityError

from sample import _, database
from sample.database import Word, Meaning, WordMeaning
from sample.repository import WordRepository, MeaningRepository, WordMeaningRepository

_logger: Logger = logging.getLogger(__name__)


class ProposedMeaningService:
    """
    Provides complex transaction-based proposed meaning actions.
    """
    _word_or_meaning_not_found_text = _('Не знаю значений этого слова.')

    @staticmethod
    def create_proposed_meaning(word: str, meaning: str, proposed_by: str) -> None:
        """
        Adds a new word-meaning pair if it's not already proposed for given russian word, its meaning and username.

        :param word: the given russian word
        :param meaning: the proposed meaning of the given russian word
        :param proposed_by: username
        """
        _logger.info('create_proposed_meaning started for word: %s, meaning: %s, proposed_by: %s',
                     word, meaning, proposed_by)
        with database.get_session() as session:
            word_object: Word = Word(value=word)
            session.add(word_object)
            try:
                _logger.info('Trying to create word %s', word)
                session.commit()
            except IntegrityError:
                _logger.info('The word %s already exists', word)
                session.rollback()
            else:
                _logger.info('The word %s created successfully', word)

            meaning_object: Meaning = Meaning(value=meaning)
            session.add(meaning_object)
            try:
                _logger.info('Trying to create meaning %s', meaning)
                session.commit()
            except IntegrityError:
                _logger.info('The meaning %s already exists', meaning)
                session.rollback()
            else:
                _logger.info('The meaning %s created successfully', meaning)

            word_object = WordRepository.select_by_value(word)
            meaning_object = MeaningRepository.select_by_value(meaning)
            proposed_meaning_object: WordMeaning = \
                WordMeaning(word_id=word_object.id, meaning_id=meaning_object.id, proposed_by=proposed_by)
            session.add(proposed_meaning_object)
            try:
                _logger.info('Trying to create word-meaning pair: %s', proposed_meaning_object)
                session.commit()
            except IntegrityError as e:
                _logger.info('The word-meaning pair already exists: %s', proposed_meaning_object)
                session.rollback()
                raise e
            else:
                _logger.info('The word-meaning pair created successfully: %s', proposed_meaning_object)

    @staticmethod
    def approve_meaning(word_meaning_object: WordMeaning) -> None:
        """
        Switches the approved flag for a given word-meaning pair object, allowing the meaning to be observed by users.

        :param word_meaning_object: the given word-meaning pair object
        """
        _logger.info('approve_meaning started for word-meaning pair: %s', word_meaning_object)
        with database.get_session() as session:
            word_meaning_object.approved = True
            session.merge(word_meaning_object)
            session.commit()

    @staticmethod
    def delete_meaning(word_meaning_object: WordMeaning) -> None:
        """
        Removes a given disapproved word-meaning pair from the database.

        :param word_meaning_object: the given word-meaning pair object
        """
        _logger.info('delete_meaning started for word-meaning pair: %s', word_meaning_object)
        with database.get_session() as session:
            word_meaning_object = session.merge(word_meaning_object)
            session.delete(word_meaning_object)
            session.commit()

    @staticmethod
    def get_approved_meanings_reply(word: str) -> str:
        """
        Constructs and returns combined meanings in one message for a given russian word.

        :param word: the given russian word
        :return: combined message with meanings
        """
        _logger.info('get_approved_meanings_reply started for word: %s', word)
        meanings: list[str] = [word_meaning.meaning.value
                               for word_meaning in WordMeaningRepository.select_all_approved_for_word(word)]
        if not meanings:
            return ProposedMeaningService._word_or_meaning_not_found_text
        elif len(meanings) == 1:
            return f'{word}:\n\n{meanings[0]}'

        combined_meanings = '\n'.join(f'{i + 1}. {m}' for i, m in enumerate(meanings))
        return f'{word}:\n\n{combined_meanings}'
