"""
Entry point of the dictionary bot admin console.
"""
import logging
from logging import Logger

from ttkbootstrap import Window

from sample.console.console_window import AdminConsole

_logger: Logger = logging.getLogger(__name__)


def main():
    """
    Launches the application.
    """
    window: Window = AdminConsole()
    _logger.info('Admin console application started')
    window.mainloop()


if __name__ == '__main__':
    main()
