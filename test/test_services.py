from unittest import TestCase
from unittest.mock import patch

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from sample import database
from sample.database import Word, Meaning, WordMeaning
from sample.repository import WordMeaningRepository
from sample.services import ProposedMeaningService
from test import drop_database, create_test_engine

_engine = create_test_engine()


@patch.object(database, 'get_session', return_value=Session(_engine))
class TestServices(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        with Session(_engine) as session:
            session.add_all([
                Word(value='тикток'),
                Word(value='обвал'),
                Word(value='растение'),
                Word(value='цветок'),
                Meaning(value='социальная сеть для обмена короткими видеороликами'),
                Meaning(value='один видеоролик из этой же соцсети'),
                Meaning(value='см. обвалиться'),
                Meaning(value='снежные глыбы или обломки скал, обрушившиеся с гор'),
                Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
                (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
                Meaning(value='орган размножения растений с венчиком из лепестков вокруг пестика и тычинок'),
                Meaning(value='''травянистое растение, в пору цветения имеющее яркую, часто ароматную, 
                распускающуюся из бутона головку или соцветие''')
            ])
            session.commit()
            session.add_all([
                WordMeaning(word_id=1, meaning_id=1, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=2, meaning_id=3, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=2, meaning_id=4, proposed_by='@testunit', approved=True),
                WordMeaning(word_id=3, meaning_id=5, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=4, meaning_id=6, proposed_by='@testunit', approved=False),
                WordMeaning(word_id=4, meaning_id=7, proposed_by='@unittest', approved=False)
            ])
            session.commit()

    @classmethod
    def tearDownClass(cls) -> None:
        drop_database(_engine)

    def test_create_proposed_meaning_existing_word_existing_meaning(self, fake_session):
        def filter_object(word_meaning_object: WordMeaning) -> bool:
            return word_meaning_object.word_id == 1 and word_meaning_object.meaning_id == 2 \
                and word_meaning_object.proposed_by == '@testunit'

        ProposedMeaningService.create_proposed_meaning('тикток', 'один видеоролик из этой же соцсети', '@testunit')
        self.assertEqual(3, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(1, len(list(filter(filter_object, word_meaning_objects))))

    def test_create_proposed_meaning_no_word_existing_meaning(self, fake_session):
        def filter_object(word_meaning_object: WordMeaning) -> bool:
            return word_meaning_object.word.value == 'зелень' and word_meaning_object.meaning_id == 5 \
                and word_meaning_object.proposed_by == '@testunit'

        ProposedMeaningService.create_proposed_meaning(
            'зелень',
            '''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
                (в отличие от животных) из воздуха (путем фотосинтеза) и почвы''',
            '@testunit')
        self.assertEqual(3, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(1, len(list(filter(filter_object, word_meaning_objects))))

    def test_create_proposed_meaning_existing_word_no_meaning(self, fake_session):
        def filter_object(word_meaning_object: WordMeaning) -> bool:
            return word_meaning_object.word_id == 4 \
                and word_meaning_object.meaning.value \
                == 'яркая, часто ароматная головка или соцветие на стебле такого растения' \
                and word_meaning_object.proposed_by == '@unittest'

        ProposedMeaningService.create_proposed_meaning(
            'цветок',
            'яркая, часто ароматная головка или соцветие на стебле такого растения',
            '@unittest')
        self.assertEqual(3, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(1, len(list(filter(filter_object, word_meaning_objects))))

    def test_create_proposed_meaning_no_word_no_meaning(self, fake_session):
        def filter_object(word_meaning_object: WordMeaning) -> bool:
            return word_meaning_object.word.value == 'трава' \
                and word_meaning_object.meaning.value == 'о чем-н. не имеющем вкуса, безвкусном (разг.)' \
                and word_meaning_object.proposed_by == '@unittest'

        ProposedMeaningService.create_proposed_meaning(
            'трава', 'о чем-н. не имеющем вкуса, безвкусном (разг.)', '@unittest')
        self.assertEqual(3, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(1, len(list(filter(filter_object, word_meaning_objects))))

    def test_create_proposed_meaning_already_existing(self, fake_session):
        with self.assertRaises(IntegrityError):
            ProposedMeaningService.create_proposed_meaning(
                'тикток', 'социальная сеть для обмена короткими видеороликами', '@unittest')
        self.assertEqual(3, fake_session.call_count)

    def test_approve_meaning(self, fake_session):
        ProposedMeaningService.approve_meaning(
            WordMeaning(word_id=4, meaning_id=6, proposed_by='@testunit', approved=False))
        self.assertEqual(1, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_all_approved_for_word('цветок')
        self.assertCountEqual(
            ['орган размножения растений с венчиком из лепестков вокруг пестика и тычинок'],
            [obj.meaning.value for obj in word_meaning_objects])

    def test_delete_meaning(self, fake_session):
        def filter_object(word_meaning_object: WordMeaning) -> bool:
            return word_meaning_object.word_id == 4 and word_meaning_object.meaning_id == 7

        ProposedMeaningService.delete_meaning(
            WordMeaning(word_id=4, meaning_id=7, proposed_by='@unittest', approved=False))
        self.assertEqual(1, fake_session.call_count)
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(0, len(list(filter(filter_object, word_meaning_objects))))

    def test_get_approved_meanings_reply_nothing_found(self, fake_session):
        self.assertEqual(ProposedMeaningService._word_or_meaning_not_found_text,
                         ProposedMeaningService.get_approved_meanings_reply('трава'))
        self.assertEqual(1, fake_session.call_count)

    def test_get_approved_meanings_reply_one_found(self, fake_session):
        self.assertEqual('тикток:\n\nсоциальная сеть для обмена короткими видеороликами',
                         ProposedMeaningService.get_approved_meanings_reply('тикток'))
        self.assertEqual(1, fake_session.call_count)

    def test_get_approved_meanings_reply_many_found(self, fake_session):
        expected_messages: list[str] = [
            'обвал:\n\n1. см. обвалиться\n2. снежные глыбы или обломки скал, обрушившиеся с гор',
            'обвал:\n\n1. снежные глыбы или обломки скал, обрушившиеся с гор\n2. см. обвалиться'
        ]
        self.assertIn(ProposedMeaningService.get_approved_meanings_reply('обвал'), expected_messages)
        self.assertEqual(1, fake_session.call_count)
