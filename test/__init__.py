from sqlalchemy import create_engine
from sqlalchemy.engine import Engine

from sample.database import Base


def create_test_engine():
    engine: Engine = create_engine('sqlite:///:memory:')
    Base.metadata.create_all(engine)
    return engine


def drop_database(engine):
    Base.metadata.reflect(engine)
    Base.metadata.drop_all(engine)
