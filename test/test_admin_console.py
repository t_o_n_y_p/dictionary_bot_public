from unittest import TestCase
from unittest.mock import patch

from ttkbootstrap.dialogs import Messagebox

from sample import _
from sample.console.console_window import AdminConsole
from sample.database import WordMeaning, Word, Meaning
from sample.repository import WordMeaningRepository
from sample.services import ProposedMeaningService


@patch.object(Messagebox, 'show_error')
class TestAdminConsole(TestCase):
    _small_meanings_list = [
        WordMeaning(
            word=Word(value='растение'),
            meaning=Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
                (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
            proposed_by='@unittest'),
        WordMeaning(
            word=Word(value='зелень'),
            meaning=Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
            (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
            proposed_by='@testunit'),
        WordMeaning(
            word=Word(value='зелень'),
            meaning=Meaning(value='зеленый цвет, зеленая краска, нечто зеленое'),
            proposed_by='@unittest')
    ]
    _normal_meanings_list = [
        WordMeaning(
            word=Word(value='тикток'),
            meaning=Meaning(value='социальная сеть для обмена короткими видеороликами'),
            proposed_by='@testunit'),
        WordMeaning(
            word=Word(value='растение'),
            meaning=Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
                (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
            proposed_by='@unittest'),
        WordMeaning(
            word=Word(value='зелень'),
            meaning=Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
            (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
            proposed_by='@testunit'),
        WordMeaning(
            word=Word(value='зелень'),
            meaning=Meaning(value='зеленый цвет, зеленая краска, нечто зеленое'),
            proposed_by='@unittest'),
        WordMeaning(
            word=Word(value='обвал'),
            meaning=Meaning(value='снежные глыбы или обломки скал, обрушившиеся с гор'),
            proposed_by='@unittest')
    ]
    _admin_console: AdminConsole = AdminConsole()

    @patch.object(WordMeaningRepository, 'select_proposed_with_limit', side_effect=Exception())
    def test_fetch_data_exception(self, fake_select_proposed_with_limit, fake_show_error):
        table_rows_before_fetch: int = len(self._admin_console._table._table_rows)
        self._admin_console._fetch_data()
        self.assertEqual(1, fake_show_error.call_count)
        self.assertEqual(1, fake_select_proposed_with_limit.call_count)
        self.assertEqual(((self._admin_console._records, ), ), fake_select_proposed_with_limit.call_args)
        self.assertEqual(table_rows_before_fetch, len(self._admin_console._table._table_rows))

    def test_fetch_data(self, fake_show_error):
        for data_set in [self._small_meanings_list, self._normal_meanings_list]:
            with self.subTest(data_set=data_set), \
                    patch.object(WordMeaningRepository, 'select_proposed_with_limit', return_value=data_set) \
                            as fake_select_proposed_with_limit:
                self._admin_console._fetch_data()
                self.assertEqual(0, fake_show_error.call_count)
                self.assertEqual(1, fake_select_proposed_with_limit.call_count)
                self.assertEqual(((self._admin_console._records, ), ), fake_select_proposed_with_limit.call_args)
                self.assertEqual(len(data_set), len(self._admin_console._table._table_rows))
                for row, word_meaning_object in zip(self._admin_console._table._table_rows, data_set):
                    self.assertEqual(row._word_label.cget('text'), word_meaning_object.word.value)
                    proposed_by_text: str = _("Предложил: {0}").format(word_meaning_object.proposed_by)
                    self.assertEqual(
                        row._meaning_label.cget('text'),
                        f'{word_meaning_object.meaning.value}\n\n{proposed_by_text}')

    def test_approve_meaning(self, fake_show_error):
        for index in [0, len(self._normal_meanings_list) - 1]:
            with self.subTest(meaning=self._normal_meanings_list[index]),\
                    patch.object(WordMeaningRepository, 'select_proposed_with_limit',
                                 return_value=self._normal_meanings_list) \
                            as fake_select_proposed_with_limit, \
                    patch.object(ProposedMeaningService, 'approve_meaning') as fake_approve_meaning:
                self._admin_console._fetch_data()
                self._admin_console._table._table_rows[index]._approve()
                self.assertEqual(0, fake_show_error.call_count)
                self.assertEqual(1, fake_approve_meaning.call_count)
                self.assertEqual(((self._normal_meanings_list[index], ), ), fake_approve_meaning.call_args)
                self.assertEqual(2, fake_select_proposed_with_limit.call_count)

    def test_reject_meaning(self, fake_show_error):
        for index in [0, len(self._normal_meanings_list) - 1]:
            with self.subTest(meaning=self._normal_meanings_list[index]),\
                    patch.object(WordMeaningRepository, 'select_proposed_with_limit',
                                 return_value=self._normal_meanings_list) \
                            as fake_select_proposed_with_limit, \
                    patch.object(ProposedMeaningService, 'delete_meaning') as fake_delete_meaning:
                self._admin_console._fetch_data()
                self._admin_console._table._table_rows[index]._reject()
                self.assertEqual(0, fake_show_error.call_count)
                self.assertEqual(1, fake_delete_meaning.call_count)
                self.assertEqual(((self._normal_meanings_list[index], ), ), fake_delete_meaning.call_args)
                self.assertEqual(2, fake_select_proposed_with_limit.call_count)

    @patch.object(WordMeaningRepository, 'select_proposed_with_limit', return_value=_normal_meanings_list)
    @patch.object(ProposedMeaningService, 'approve_meaning', side_effect=Exception())
    def test_approve_meaning_exception(self, fake_approve_meaning, fake_select_proposed_with_limit, fake_show_error):
        self._admin_console._fetch_data()
        self._admin_console._table._table_rows[0]._approve()
        self.assertEqual(1, fake_show_error.call_count)
        self.assertEqual(1, fake_approve_meaning.call_count)
        self.assertEqual(((self._normal_meanings_list[0], ), ), fake_approve_meaning.call_args)
        self.assertEqual(1, fake_select_proposed_with_limit.call_count)

    @patch.object(WordMeaningRepository, 'select_proposed_with_limit', return_value=_normal_meanings_list)
    @patch.object(ProposedMeaningService, 'delete_meaning', side_effect=Exception())
    def test_reject_meaning_exception(self, fake_delete_meaning, fake_select_proposed_with_limit, fake_show_error):
        index = len(self._normal_meanings_list) - 1
        self._admin_console._fetch_data()
        self._admin_console._table._table_rows[index]._reject()
        self.assertEqual(1, fake_show_error.call_count)
        self.assertEqual(1, fake_delete_meaning.call_count)
        self.assertEqual(((self._normal_meanings_list[index], ), ), fake_delete_meaning.call_args)
        self.assertEqual(1, fake_select_proposed_with_limit.call_count)
